

#ifndef _CANINOS_CAMERA_H_
#define _CANINOS_CAMERA_H_

#include <linux/device.h>
#include <linux/videodev2.h>
#include <media/drv-intf/soc_mediabus.h>

#define owl_camera_hw_call(hw,f,args...)  ((!(hw)) ? -ENODEV : (((hw)->ops->f) ? ((hw)->ops->f((hw), ##args)) : -ENOIOCTLCMD))

/* Definition for si_platform_data */
#define SI_DATAWIDTH_8				0x01
#define SI_DATAWIDTH_10			    0x02

#define SI_CHANNEL_0 0
#define SI_CHANNEL_1 1


enum owl_dev_state {
    DEV_STOP = 0,
    DEV_START,
    DEV_SUSPEND,
    DEV_RESUME,
    DEV_OPEN,
    DEV_CLOSE,
    DEV_WAITING,
};

struct owl_camera_hw_adapter;

struct owl_camera_reg{
	volatile void * reg;
	u32 val;
};
extern volatile void*	si_clk;

//extern void*	noc_si_to_ddr;	
//extern void*	gpio_dinen;
//extern void* si_reset;



struct owl_camera_hw_ops
{
	int (*hw_adapter_init)(struct owl_camera_hw_adapter* hw,struct platform_device *pdev);
	int (*hw_adapter_exit)(struct owl_camera_hw_adapter* hw,struct platform_device *pdev);
    int (*get_channel_state)(struct owl_camera_hw_adapter* hw, int channel);
    int (*set_channel_if)(struct owl_camera_hw_adapter* hw, int channel, int bus_type);
    int (*set_channel_addrY)(struct owl_camera_hw_adapter* hw, int channel, void *addrY);
    int (*set_channel_addrU)(struct owl_camera_hw_adapter* hw, int channel, void *addrU);
    int (*set_channel_addrV)(struct owl_camera_hw_adapter* hw, int channel, void *addrV);
    int (*set_channel_addr1UV)(struct owl_camera_hw_adapter* hw, int channel, void *addr1UV);
    int (*set_channel_input_fmt)(struct owl_camera_hw_adapter* hw, int channel, unsigned int pixel_code);
    int (*set_channel_output_fmt)(struct owl_camera_hw_adapter* hw, int channel, u32 fourcc);
    int (*set_channel_preline)(struct owl_camera_hw_adapter* hw, int channel, int preline);
	int (*set_channel_en)(struct owl_camera_hw_adapter* hw, int channel);
    int (*clear_channel_en)(struct owl_camera_hw_adapter* hw, int channel);
    int (*set_channel_preline_int_en)(struct owl_camera_hw_adapter* hw, int channel);
    int (*set_channel_frameend_int_en)(struct owl_camera_hw_adapter* hw, int channel);
	int (*clear_channel_frameend_int_en)(struct owl_camera_hw_adapter* hw, int channel);
	int (*clear_all_pending)(struct owl_camera_hw_adapter* hw);
	int (*clear_channel_preline_int_en)(struct owl_camera_hw_adapter* hw, int channel);
    int (*clear_channel_preline_pending)(struct owl_camera_hw_adapter* hw, int channel);
    int (*clear_channel_frameend_pending)(struct owl_camera_hw_adapter* hw, int channel);
	int (*set_signal_polarity)(struct owl_camera_hw_adapter* hw,int channel,unsigned int common_flags);
	int (*set_col_range)(struct owl_camera_hw_adapter* hw,int channel,unsigned int start,unsigned int end);
	int (*set_row_range)(struct owl_camera_hw_adapter* hw,int channel,unsigned int start,unsigned int end); 
	int (*get_channel_overflow)(struct owl_camera_hw_adapter* hw, int channel);
	int (*clear_channel_overflow)(struct owl_camera_hw_adapter* hw, int channel); 
	int (*save_regs)(struct owl_camera_hw_adapter* hw);
	int (*restore_regs)(struct owl_camera_hw_adapter* hw); 
 	int (*dump_isp_reg)(struct owl_camera_hw_adapter* hw, int channel);	
  	void (*pcm1_multipin_en)(struct owl_camera_hw_adapter* hw, int channel);	
  	//int (*mipi_csi_init)(struct owl_camera_hw_adapter* hw,struct caninos_si_dev *sidev);
};


struct owl_camera_hw_adapter {
    char *hw_name;
	struct owl_camera_reg *restored_regs;
	int restored_regs_num;
    //struct owl_camera_dev *cam_dev;

    int max_channel;
    int has_isp;
    int is_3D_support;
    int crop_x_align;
    int crop_y_align;
    int crop_w_align;
    int crop_h_align;
    int max_width;
    int max_height;
	int preline_int_pd[2];
	int frameend_int_pd[2];

    int isp_state;

    int power_ref;
    int enable_ref;

    struct clk *hw_clk;

    struct owl_camera_hw_ops *ops;

    void *priv;

};

struct si_dma_desc {
	struct list_head list;
	struct fbd *p_fbd;
	dma_addr_t fbd_phys;
};

struct si_platform_data {
	u8 has_emb_sync;
	u8 hsync_act_low;
	u8 vsync_act_low;
	u8 pclk_act_falling;
	u8 full_mode;
	u32 data_width_flags;
	/* Using for ISI_CFG1 */
	u32 frate;
};

struct si_graph_entity {
	struct device_node *node;

	struct v4l2_async_subdev asd;
	struct v4l2_subdev *subdev;
};

/*
 * struct si_format - SI media bus format information
 * @fourcc:		Fourcc code for this format
 * @mbus_code:		V4L2 media bus format code.
 * @bpp:		Bytes per pixel (when stored in memory)
 * @swap:		Byte swap configuration value
 * @support:		Indicates format supported by subdev
 * @skip:		Skip duplicate format supported by subdev
 */
struct si_format {
	u32	fourcc;
	u32	mbus_code;
	u8	bpp;
	//u32	swap;
};

struct caninos_camera_param {
    /* ISP data offsets within croped by the  camera output */
    unsigned int isp_left;
    unsigned int isp_top;
    /* Client output, as seen */
    unsigned int width;
    unsigned int height;
    /*
     * User window from S_CROP / G_CROP, produced by client cropping,
     * GL5203 cropping, mapped back onto the client
     * input window
     */
    struct v4l2_rect subrect;
    /* Camera cropping rectangle */
    struct v4l2_rect rect;
    const struct soc_mbus_pixelfmt *extra_fmt;
    unsigned int pixel_code;
    unsigned long flags;
    unsigned int skip_frames;
    //int channel;
    enum v4l2_mbus_type bus_type;
    int lane_num;
    int raw_width;
    int data_type;//0 for yuv sensor,1 for raw-bayer sensor
    int (*ext_cmd)(struct v4l2_subdev *sd,int cmd,void *args);
};


/*
 * Caninos has two channels
 */
struct caninos_si_dev {

    unsigned    int         dvp_mbus_flags;
    u16 	                width_flags;	/* max 12 bits */

    struct      list_head   video_buffer_list;
    spinlock_t              lock;            /* Protects video buffer lists */
    struct      mutex		mutex_lock;
    struct      vb2_buffer  *cur_frm;
    struct      vb2_buffer  *prev_frm;
    struct      v4l2_format fmt;
    struct      frame_buffer *active;
    struct v4l2_rect		bounds;

    struct      device      *dev;
    struct      video_device		*vdev;
    //struct      media_device *mdev;
    unsigned    int         sequence;
    struct      completion  complete;

    int                     irq;

    int                     status;
    struct      pinctrl     *mfp;
    int                     skip_frames;

    //struct sensor_pwd_info spinfo;
    //struct isp_regulators ir;
    struct      clk         *si_clk;
    struct      clk         *csi_clk;
    struct      clk         *ch_clk[2]; /* correspond to SI_CHANNEL_0/1 (should be 0 or 1) */
	struct      owl_camera_hw_adapter *hw_adapter;
    struct      reset_control *rst;

    const       struct      si_format		*current_fmt;
    unsigned    int         num_user_formats;
    const   struct si_format    **user_formats;

    unsigned    int         channel;//channel over sensor
    unsigned     int        ds; //ds do not exist in k5

    struct  caninos_camera_param *cam_param;
    struct  si_graph_entity		entity;
    struct  v4l2_async_notifier	notifier;
    struct  vb2_queue		queue;
    struct  v4l2_device		v4l2_dev;

    struct si_platform_data	pdata;

    /* Allocate descriptors for dma buffer use */
	struct fbd			*p_fb_descriptors;
	dma_addr_t			fb_descriptors_phys;
	struct	list_head dma_desc_head;
	struct si_dma_desc		dma_desc[VIDEO_MAX_FRAME];
	bool				enable_preview_path;

    /* Ensure DMA operations atomicity */
	struct mutex			dma_lock;

    struct media_device		mdev;
	struct media_pad		vid_cap_pad;

    struct v4l2_rect		crop;
	bool				do_crop;


    //try this gpios
    int power_gpio;
    int reset_gpio;
    int standby_gpio;
};




#define debug_print(fmt, arg...) do { if (1) printk(KERN_ERR fmt, ##arg); } while (0)


extern struct owl_camera_hw_adapter atm7059_hw_adapter;
//extern struct owl_camera_hw_adapter atm7039_hw_adapter;
#endif
