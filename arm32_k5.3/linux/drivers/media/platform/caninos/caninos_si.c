// SPDX-License-Identifier: GPL-2.0-only
/*
 * Igor Ruschi Andrade E LIma, <igor.lima@lsitec.org.br>
 *
 * Based on previous work by Lars Haring, <lars.haring@atmel.com>
 * and Sedji Gaouaou and Josh Wu, <josh.wu@atmel.com>
 * Based on the atmel-isi driver with respective copyright holders
 */

/*
 * V4L2 Driver for caninos camera host
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/clk.h>
#include <linux/completion.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of_graph.h>
#include <linux/platform_device.h>
#include <linux/pm_runtime.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/reset.h>

#include <linux/videodev2.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-device.h>
#include <media/v4l2-dev.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-event.h>
#include <media/v4l2-fwnode.h>
#include <media/videobuf2-dma-contig.h>
#include <media/v4l2-image-sizes.h>
#include <media/v4l2-rect.h>

#include <media/videobuf2-memops.h>
#include <media/v4l2-common.h>

#include <linux/wait.h>
#include <linux/time.h>
#include <linux/sched.h>

#include <mach/hardware.h>
#include <mach/module-owl.h>
#include <mach/clkname.h>
#include <mach/powergate.h>
#include <atc260x/atc260x.h>

#include <linux/gpio.h>
#include <linux/of_gpio.h>


#include "caninos_si.h"
#include"atm7059.c"

//#include <linux/version.h>
//#include <linux/vmalloc.h>
//#include <media/drv-intf/soc_mediabus.h>
//#include <linux/v4l2-mediabus.h>
//#include <media/videobuf2-v4l2.h>
//#include <asm/delay.h>
//#include <linux/pinctrl/consumer.h>
//#include <linux/mfd/atc260x/atc260x.h>


//static struct owl_camera_hw_adapter* hw_adapter = NULL;

//#define pr_err(fmt, args...)   printk(KERN_ERR"[owl_camera] (ERR) line:%d--%s() "fmt"\n", __LINE__, __FUNCTION__, ##args)
//#ifdef OWL_DBG
//#define pr_info(fmt, args...)  printk(KERN_INFO"[owl_camera] (ERR) line:%d--%s() "fmt"\n", __LINE__, __FUNCTION__, ##args) 
//#else 
//#define pr_info(fmt, args...)  do{}while(0)
//#endif

#define SI_MAX_WIDTH	4288U
#define SI_MAX_HEIGHT	3000U
#define SI_WORK_CLOCK	60000000        //Hz
//#define ISP_WORK_CLOCK        120000000
#define CSI_WORK_CLOCK  150000000


#define MAX_VIDEO_MEM 32
#define DROP_FRAM_INIT_VAL  0xff



#define SI_MIN_FRAME_RATE 2
#define SI_FRAME_INTVL_MSEC	(1000 / SI_MIN_FRAME_RATE)

#ifdef MCU_K7
//powergate stuffs of k7
#define SPS_PG 0xE01B0100
#define PWR_VCE (0x1 << 1)
#endif


/*Frame buffer data */
struct frame_buffer {
    struct vb2_v4l2_buffer vb;
	bool prepared;
    //struct si_dma_desc *p_dma_desc;
	dma_addr_t	paddr;
    struct list_head list;
	size_t size;
    unsigned int pixel_code;
};

static void reset_si_module(void){
	int val;
	void __iomem *reg = ioremap(0xb0160000+0xA8,4);
	val = readl(reg);
	val &= ~(1<<12);
	writel(val, reg);
	mdelay(2);
	val = readl(reg);
	val |= (1<<12);
	writel(val, reg);
	pr_info("SI module reseted");
}

static inline void caninos_si_set_preline(struct caninos_si_dev *sidev)
{
  	int ret = 0;
	pr_info("%s:init", __func__);
	pr_info("channel:%d, top:%d", sidev->channel, sidev->cam_param->isp_top);
	
	ret = owl_camera_hw_call(sidev->hw_adapter, set_channel_preline, sidev->channel, sidev->cam_param->isp_top);
}

static inline int caninos_si_set_col_range(struct caninos_si_dev *sidev, unsigned int start,
                                    unsigned int end)
{
    int width;
 	int ret = 0;
	 pr_info("%s: init, start=%u, end=%u", __func__, start, end);
    start = ((start + 1) / 2) * 2;
    width = end - start + 1;
    width = ((width + 31) / 32) * 32;
    end = width + start - 1;

    if (width >= SI_MAX_WIDTH) {
        pr_err("width is over range, width:%d, start:%d, end:%d", width, start, end);
        return -EINVAL;
    }
	ret = owl_camera_hw_call(sidev->hw_adapter, set_col_range, sidev->channel, start, end);
	
    return 0;
}

static inline int caninos_si_set_row_range(struct caninos_si_dev *sidev, unsigned int start,
                                    unsigned int end)
{
    int height = end - start + 1;
	int ret = 0;
	pr_info("%s: init", __func__);
    height = ((height + 1) / 2) * 2;
    end = height + start - 1;

   // pr_info("height:%d, start:%d, end:%d", height, start, end);
    if (height > SI_MAX_HEIGHT) {
        pr_err("height is over range, height:%d, start:%d, end:%d", height, start, end);
        return -EINVAL;
    }
	ret = owl_camera_hw_call(sidev->hw_adapter, set_row_range,sidev->channel, start, end);
	
    return 0;
}

/* rect is guaranteed to not exceed the camera rectangle */
static inline void caninos_si_set_rect(struct caninos_si_dev *sidev)
{
	pr_info("%s:init; isp_left=%u, pix->width", __func__, sidev->cam_param->isp_left, sidev->fmt.fmt.pix.width);
    caninos_si_set_col_range(sidev, sidev->cam_param->isp_left, sidev->cam_param->isp_left + sidev->fmt.fmt.pix.width - 1);
    caninos_si_set_row_range(sidev, sidev->cam_param->isp_top, sidev->cam_param->isp_top + sidev->fmt.fmt.pix.height - 1);
}

#ifdef MCU_K7
static void vce_si_powergate_enable(bool enable)
{
    void __iomem *reg_addr;
    u32 sps_pg_ctl;
    reg_addr = ioremap(SPS_PG, 4);
    sps_pg_ctl = readl(reg_addr);
    if(enable){
        sps_pg_ctl |= PWR_VCE;
    }else
    {
        sps_pg_ctl &= ~(PWR_VCE); 
    }
    writel(sps_pg_ctl, reg_addr);
    //sps_pg_ctl = readl(reg_addr);
    iounmap(reg_addr);
    //return sps_pg_ctl;
}
#endif


static int caninos_si_set_input_fmt(struct caninos_si_dev *sidev, unsigned int pixel_code)
{	
	pr_info("%s: init", __func__);
    return owl_camera_hw_call(sidev->hw_adapter, set_channel_input_fmt, sidev->channel, pixel_code);
}

static int caninos_si_set_output_fmt(struct caninos_si_dev *sidev, u32 fourcc)
{
   	int ret = 0; 
	pr_info("%s: init", __func__);
    ret = owl_camera_hw_call(sidev->hw_adapter, set_channel_output_fmt, sidev->channel, fourcc);
    return ret;
}

static void caninos_si_start_dma(struct caninos_si_dev *sidev, struct vb2_buffer *vb)
{
	dma_addr_t isp_addr;
    dma_addr_t isp_addr_Y;
    dma_addr_t isp_addr_U;
    dma_addr_t isp_addr_V;
    dma_addr_t isp_addr_UV;
	u32 fourcc = sidev->current_fmt->fourcc;
	struct vb2_v4l2_buffer *vbuf = to_vb2_v4l2_buffer(vb);
	//struct caninos_si_dev *sidev = vb2_get_drv_priv(vb->vb2_queue);
	struct frame_buffer *buf = container_of(vbuf, struct frame_buffer, vb);
	pr_info("%s: init", __func__);
    
	if(!buf->prepared)
	{
		dev_err(sidev->dev,"framebuffer is not prepared");
		pr_info("%s: preparing buffer",__func__);
		buf->paddr =
			vb2_dma_contig_plane_dma_addr(&buf->vb.vb2_buf, 0);
		buf->size = vb2_plane_size(&buf->vb.vb2_buf, 0);
		buf->prepared = true;

		vb2_set_plane_payload(&buf->vb.vb2_buf, 0, buf->size);

		dev_dbg(sidev->dev, "buffer[%d] phy=%pad size=%zu\n",
			vb->index, &buf->paddr, buf->size);
	}else{
		pr_info("%s: buffer already prepared",__func__);
		switch (buf->vb.vb2_buf.memory) {
		case V4L2_MEMORY_MMAP:
			//isp_addr = vb2_dma_contig_plane_dma_addr(vb, 0);
			pr_info("MMAP memory type");
			break;
		case V4L2_MEMORY_USERPTR:
			pr_info("USERPTR memory type");
			isp_addr = vb->planes[0].m.userptr;
			break;
		case V4L2_MEMORY_DMABUF:
			pr_info("DMA memory type");
			break;
		default:
			pr_err("wrong memory type. %d", vb->memory);
			return;
		}
		//vb2_buffer infos
		pr_info("%s: vb2_buffer.index = %d", __func__, buf->vb.vb2_buf.index);
		pr_info("%s: vb2_buffer.type = 0x%x", __func__, buf->vb.vb2_buf.type);
		pr_info("%s: vb2_buffer.num_planes = %d", __func__, buf->vb.vb2_buf.num_planes);
		pr_info("%s: vb2_buffer.state = 0x%x", __func__, buf->vb.vb2_buf.state);
		
		//planes info
		pr_info("%s: vb2_buffer->planes[0].bytesused = %d", __func__, buf->vb.vb2_buf.planes[0].bytesused);
		pr_info("%s: vb2_buffer->planes[0].length = %d", __func__, buf->vb.vb2_buf.planes[0].length);
		pr_info("%s: vb2_buffer->planes[0].data_offset = 0x%x", __func__, buf->vb.vb2_buf.planes[0].data_offset);
		pr_info("%s: vb2_buffer->planes[0].m.offset = 0x%x", __func__, buf->vb.vb2_buf.planes[0].m.offset);
	}

	isp_addr = buf->paddr;
	

    if (NULL == vb) {
        pr_err( "cannot get video buffer.");
        return;
    }

	//pr_info("frame paddr :0x%x", isp_addr);

    switch (fourcc) {
    case V4L2_PIX_FMT_YUV420:      //420 planar
        pr_info("YUV 420p");
        isp_addr_Y = ALIGN(isp_addr, 2);
        isp_addr_U = ALIGN(isp_addr_Y + sidev->fmt.fmt.pix.width * sidev->fmt.fmt.pix.height, 2);
        isp_addr_V = ALIGN(isp_addr_U + sidev->fmt.fmt.pix.width * sidev->fmt.fmt.pix.height / 4, 2);
		owl_camera_hw_call(sidev->hw_adapter, set_channel_addrY, sidev->channel, (void*)isp_addr_Y);
		owl_camera_hw_call(sidev->hw_adapter, set_channel_addrU, sidev->channel, (void*)isp_addr_U);
		owl_camera_hw_call(sidev->hw_adapter, set_channel_addrV, sidev->channel, (void*)isp_addr_V);
     
        break;
	 case V4L2_PIX_FMT_YVU420:      //420 planar
        pr_info("YVU 420p");
        isp_addr_Y = ALIGN(isp_addr, 2);
        isp_addr_U = ALIGN(isp_addr_Y + sidev->fmt.fmt.pix.width * sidev->fmt.fmt.pix.height, 2);
        isp_addr_V = ALIGN(isp_addr_U + sidev->fmt.fmt.pix.width * sidev->fmt.fmt.pix.height / 4, 2);
		owl_camera_hw_call(sidev->hw_adapter, set_channel_addrY, sidev->channel, (void*)isp_addr_Y);
		owl_camera_hw_call(sidev->hw_adapter, set_channel_addrU, sidev->channel, (void*)isp_addr_V);
		owl_camera_hw_call(sidev->hw_adapter, set_channel_addrV, sidev->channel, (void*)isp_addr_U);
     
        break;	

    case V4L2_PIX_FMT_YUV422P:     //422 semi planar
        pr_info("YUV 422p");
        isp_addr_Y = ALIGN(isp_addr, 2);
        isp_addr_U = ALIGN(isp_addr_Y + sidev->fmt.fmt.pix.width * sidev->fmt.fmt.pix.height, 2);
        isp_addr_V = ALIGN(isp_addr_U + sidev->fmt.fmt.pix.width * sidev->fmt.fmt.pix.height / 4, 2);
        owl_camera_hw_call(sidev->hw_adapter, set_channel_addrY, sidev->channel, (void*)isp_addr_Y);
        owl_camera_hw_call(sidev->hw_adapter, set_channel_addrU, sidev->channel, (void*)isp_addr_V);
        owl_camera_hw_call(sidev->hw_adapter, set_channel_addrV, sidev->channel, (void*)isp_addr_U);

        break;

    case V4L2_PIX_FMT_NV12:        //420 semi-planar
    case V4L2_PIX_FMT_NV21:        //420 semi-planar
        pr_info("NV21 420sp");
        isp_addr_Y = ALIGN(isp_addr, 2);
        isp_addr_UV = ALIGN(isp_addr_Y + sidev->fmt.fmt.pix.width * sidev->fmt.fmt.pix.height, 2);
		owl_camera_hw_call(sidev->hw_adapter, set_channel_addrY, sidev->channel, (void*)isp_addr_Y);
		owl_camera_hw_call(sidev->hw_adapter, set_channel_addrU, sidev->channel, (void*)isp_addr_UV);
      
        break;

    case V4L2_PIX_FMT_YUYV:        //interleaved
	case V4L2_PIX_FMT_UYVY:
        pr_info("UYVY 422P");
        isp_addr_Y = ALIGN(isp_addr, 2);
        isp_addr_U = ALIGN(isp_addr_Y + sidev->fmt.fmt.pix.width * sidev->fmt.fmt.pix.height, 2);
        isp_addr_V = ALIGN(isp_addr_U + sidev->fmt.fmt.pix.width * sidev->fmt.fmt.pix.height / 4, 2);
        owl_camera_hw_call(sidev->hw_adapter, set_channel_addrY, sidev->channel, (void*)isp_addr_Y);
        owl_camera_hw_call(sidev->hw_adapter, set_channel_addrU, sidev->channel, (void*)isp_addr_V);
        owl_camera_hw_call(sidev->hw_adapter, set_channel_addrV, sidev->channel, (void*)isp_addr_U);
        break;

    default:       /* Raw RGB */
        pr_err("Set isp output format failed, fourcc = 0x%x", fourcc);
        return;
    }
	pr_info("%s: isp_addr = 0x%x", __func__, isp_addr);
	pr_info("%s: isp_addr_y = 0x%x", __func__, isp_addr_Y);
	pr_info("%s: isp_add_u = 0x%x", __func__, isp_addr_U);
	pr_info("%s: isp_addr_v = 0x%x", __func__, isp_addr_V);
}


static int caninos_si_set_signal_polarity(struct caninos_si_dev *sidev, unsigned int common_flags)
{

	pr_info("channel:%d, pclk:%s, hsync:%s, vsync:%s",
		sidev->channel, 
		common_flags & V4L2_MBUS_PCLK_SAMPLE_FALLING ? "falling" : "rising",
		common_flags & V4L2_MBUS_HSYNC_ACTIVE_LOW ? "low" : "high",
		common_flags & V4L2_MBUS_VSYNC_ACTIVE_LOW ? "low" : "high");

	if(!owl_camera_hw_call(sidev->hw_adapter,set_signal_polarity,sidev->channel,common_flags))
	{
		
		return 0;
	}

	return 0;
  
}

static int caninos_si_clean_all_pending(struct caninos_si_dev *sidev){
	pr_info("%s: init", __func__);
    return owl_camera_hw_call(sidev->hw_adapter, clear_all_pending);
}

static void caninos_si_capture_start(struct caninos_si_dev *sidev)
{
	int ret = 0;
	pr_info("%s: init", __func__);
    if (sidev->status == DEV_START) {
        pr_info("already start");
        return;
    }
	
    /*platform code should do and has done this job actually,if sens1_d0~d3 is still disabled, 
	please contact to engineer: wurui ,which is responsable for the pinctrl module.(liyuan) */
    //owl_camera_hw_call(hw_adapter, pcm1_multipin_en, cam_param->channel);
    
    if (V4L2_MBUS_CSI1 == sidev->cam_param->bus_type) { 
		//ret |= owl_camera_hw_call(sidev->hw_adapter, mipi_csi_init, sidev);
		pr_err("CSI type not supported yet");
    }

    //sidev->skip_frames = DROP_FRAM_INIT_VAL;
    caninos_si_set_preline(sidev);
	ret |= owl_camera_hw_call(sidev->hw_adapter, set_channel_if, sidev->channel, sidev->cam_param->bus_type);
	ret |= owl_camera_hw_call(sidev->hw_adapter, clear_channel_frameend_int_en, sidev->channel);
	ret |= owl_camera_hw_call(sidev->hw_adapter, set_channel_preline_int_en, sidev->channel);
	ret |= owl_camera_hw_call(sidev->hw_adapter, clear_all_pending);
	
	ret |= owl_camera_hw_call(sidev->hw_adapter, set_channel_en, sidev->channel);

	if(ret){
		pr_err("some hw_call fail err = 0x%x", ret);
	}
	
    sidev->status = DEV_START;

    pr_info("channel-%d, %s", sidev->channel,
             V4L2_MBUS_PARALLEL == sidev->cam_param->bus_type ? "parallel" : "mipi");

    
}

static void caninos_si_capture_stop(struct caninos_si_dev *sidev)
{
    unsigned long flags;
    int ret;
	pr_info("%s: init", __func__);
    if (sidev->status == DEV_STOP) {
        pr_err("already stop");
        return;
    }

    spin_lock_irqsave(&sidev->lock, flags);
	//ret |= owl_camera_hw_call(sidev->hw_adapter, clear_channel_preline_int_en, sidev->channel);
	//ret |= owl_camera_hw_call(sidev->hw_adapter, clear_channel_preline_pending,sidev->channel);
	//ret |= owl_camera_hw_call(sidev->hw_adapter, set_channel_frameend_int_en, sidev->channel);
	ret |= owl_camera_hw_call(sidev->hw_adapter, clear_channel_en, sidev->channel);
	if(ret){
		pr_err("some hw_call fail err = 0x%x", ret);
	}

    sidev->status = DEV_STOP;
    spin_unlock_irqrestore(&sidev->lock, flags);

    /*ret = wait_for_completion_timeout(&sidev->complete, msecs_to_jiffies(SI_FRAME_INTVL_MSEC));
    if (ret <= 0) {
        pr_err("%s wake up before frame complete", __func__);
    } else {
        pr_info("%s ret is %d remain %dms to wait for stop", __func__, ret,jiffies_to_msecs(ret));
	}*/
    //if (V4L2_MBUS_CSI1 == sidev->cam_param->bus_type) {
     //   mipi_disable(sidev);
   // }
}

static void caninos_si_enable_all_int(struct caninos_si_dev *sidev){
	int ret;
	pr_info("%s: init", __func__);
    ret =  owl_camera_hw_call(sidev->hw_adapter, set_channel_frameend_int_en, sidev->channel);
	if(ret){
		pr_err("error on enable frame interrupts");
	}
    ret |=  owl_camera_hw_call(sidev->hw_adapter, set_channel_preline_int_en, sidev->channel);
	if(ret){
		pr_err("error on enable preline interrupts");
	}
    return;
}


static int caninos_si_disable_all_int(struct caninos_si_dev *sidev){
    int ret;
	pr_info("%s: init", __func__);
    ret =  owl_camera_hw_call(sidev->hw_adapter, clear_channel_frameend_int_en, sidev->channel);
    ret |=  owl_camera_hw_call(sidev->hw_adapter, clear_channel_preline_int_en, sidev->channel);
    return ret;
}

/* make sure capture list is not empty */
static inline struct vb2_buffer *next_vb2_buf(struct caninos_si_dev *sidev)
{
    struct frame_buffer *fb = NULL;
	pr_info("%s: init", __func__);
    fb = list_entry(sidev->video_buffer_list.next, struct frame_buffer, list);
    list_del_init(&fb->list);

    return (&fb->vb.vb2_buf);
}


/*VIDEO BUFFER FUNCTIONS START HERE */

static int caninos_si_queue_setup(struct vb2_queue *vq, unsigned int *nbuffers,
    unsigned int *nplanes, unsigned int sizes[], struct device *alloc_dvs[])
{
	unsigned long frame_size;
    struct caninos_si_dev *sidev = vb2_get_drv_priv(vq);;
    frame_size = sidev->fmt.fmt.pix.sizeimage;
	pr_info("%s: init", __func__);
    if (frame_size < 0){
		pr_info("%s: framesize  <0");
        return (frame_size);
	}

    /* Make sure the image size is large enough. */
	if (*nplanes){
		pr_info("%s: sizes < framesize <0");
		return sizes[0] < frame_size ? -EINVAL : 0;
	}

    *nplanes = 1;
    sidev->sequence = 0;
    sizes[0] = frame_size;

    sidev->active = NULL;

    if (*nbuffers < 2) {
		pr_info(" 2 > nbuffers = %u", *nbuffers);
        *nbuffers = 2;
    }

	pr_info("%s, nbuffers=%u, size=%u\n", __func__,*nbuffers, sizes[0]);
    if (sizes[0] * *nbuffers > MAX_VIDEO_MEM * 1024 * 1024){
        *nbuffers = MAX_VIDEO_MEM * 1024 * 1024 / sizes[0];
		pr_info("%s: fix to nbuffers=%u",__func__,*nbuffers);
	}
    return 0;

}

static int caninos_si_buffer_prepare(struct vb2_buffer *vb)
{
	struct vb2_v4l2_buffer *vbuf = to_vb2_v4l2_buffer(vb);
	struct caninos_si_dev *sidev = vb2_get_drv_priv(vb->vb2_queue);
	unsigned long size;
	struct frame_buffer *buf = container_of(vbuf, struct frame_buffer, vb);
	pr_info("%s: init", __func__);
	size = sidev->fmt.fmt.pix.sizeimage;

	buf->vb.vb2_buf.state = VB2_BUF_STATE_PREPARING;
	
	if (!list_empty(&buf->list)) {
        pr_err("Buffer %p on list already!", vb);
    }

    if (vb2_plane_size(vb, 0) < size) {
		dev_err(sidev->dev, "%s data will not fit into plane (%lu < %lu)\n",
				__func__, vb2_plane_size(vb, 0), size);
		return -EINVAL;
	}

    vb2_set_plane_payload(vb, 0, size);

	if (!buf->prepared) {
		/* Get memory addresses */
		pr_info("%s: preparing buffer",__func__);
		buf->paddr =
			vb2_dma_contig_plane_dma_addr(&buf->vb.vb2_buf, 0);
		buf->size = vb2_plane_size(&buf->vb.vb2_buf, 0);
		buf->prepared = true;

		vb2_set_plane_payload(&buf->vb.vb2_buf, 0, buf->size);

		dev_info(sidev->dev, "buffer[%d] phy=%pad size=%zu\n",
			vb->index, &buf->paddr, buf->size);
	}

	return 0;

}

static void caninos_si_buffer_queue(struct vb2_buffer *vb)
{
    struct vb2_v4l2_buffer *vbuf = to_vb2_v4l2_buffer(vb);
    struct caninos_si_dev *sidev = vb2_get_drv_priv(vb->vb2_queue);
    struct frame_buffer *buf = container_of(vbuf, struct frame_buffer, vb);
    unsigned long flag;
	int ret;

	pr_info("%s: init", __func__);
    pr_info("%s (vb=0x%p) vaddr=0x%p %lu", __func__,
            vb, vb2_plane_vaddr(vb, 0), vb2_get_plane_payload(vb, 0));

    spin_lock_irqsave(&sidev->lock, flag);

	list_add_tail(&buf->list, &sidev->video_buffer_list);

	if (vb2_is_streaming(vb->vb2_queue)){
			sidev->active = buf;
			spin_unlock_irqrestore(&sidev->lock, flag);
			caninos_si_set_rect(sidev);
			caninos_si_start_dma(sidev, &buf->vb.vb2_buf);
			caninos_si_capture_start(sidev);
			buf->vb.vb2_buf.state = VB2_BUF_STATE_QUEUED;
			return;
	}else{
			pr_info("%s:vb2 is not streaming",__func__);
	}		

	spin_unlock_irqrestore(&sidev->lock, flag);

	buf->vb.vb2_buf.state = VB2_BUF_STATE_QUEUED;

}


static void caninos_si_buffer_cleanup(struct vb2_buffer *vb)
{

    struct vb2_v4l2_buffer *vbuf = to_vb2_v4l2_buffer(vb);
	struct caninos_si_dev *sidev = vb2_get_drv_priv(vb->vb2_queue);
	struct frame_buffer *buf = container_of(vbuf, struct frame_buffer, vb);
    unsigned long flag;
	//pr_info("");
	pr_info("%s: init", __func__);
    spin_lock_irqsave(&sidev->lock, flag);

    if (&sidev->active->vb.vb2_buf == vb) {
        sidev->active = NULL;
    }

    if(buf != NULL && buf->list.next != NULL) {
        //list_del_init(&buf->list);
    }          

    spin_unlock_irqrestore(&sidev->lock, flag);
	//pr_info("Exit");
}


static int caninos_si_videobuf_init(struct vb2_buffer *vb)
{
    struct vb2_v4l2_buffer *vbuf = to_vb2_v4l2_buffer(vb);
	struct frame_buffer *buf = container_of(vbuf, struct frame_buffer, vb);

	pr_info("%s: init", __func__);
	INIT_LIST_HEAD(&buf->list);

    return (0);
}

static int caninos_si_start_streaming(struct vb2_queue *vq, unsigned int count)
{
	//pr_info("");
    struct caninos_si_dev *sidev = vb2_get_drv_priv(vq);
	struct frame_buffer *buf, *node;
	int ret;

	//pm_runtime_get_sync(sidev->dev);
	pr_info("%s: init", __func__);
	/* Enable stream on the sub device */
	ret = v4l2_subdev_call(sidev->entity.subdev, video, s_stream, 1);
	if (ret && ret != -ENOIOCTLCMD) {
		pr_err("stream on failed in subdev\n");
		goto err_reset;
	}

	/* Should Reset SI ?*/
	//??
	
	spin_lock_irq(&sidev->lock);
	sidev->sequence = 0;
	reinit_completion(&sidev->complete);

	sidev->active = list_first_entry(&sidev->video_buffer_list,
					struct frame_buffer, list);

	spin_unlock_irq(&sidev->lock);

	pr_info("%s: count = %d(useless)",__func__, count);

	caninos_si_set_rect(sidev);
    caninos_si_start_dma(sidev, &sidev->active->vb.vb2_buf);
    caninos_si_capture_start(sidev);

	// Clear any pending interrupt 
	if(caninos_si_clean_all_pending(sidev))
		pr_info("Error to clean all pending");
	// enable all interrupts
	pr_info("debug interrupt enable(before function)");
    caninos_si_enable_all_int(sidev);
	caninos_si_enable_all_int(sidev);

	//Just a test
	spin_lock_irq(&sidev->lock);
	if (sidev->active) {
		struct vb2_v4l2_buffer *vbuf = &sidev->active->vb;
		buf = sidev->active;

		list_del_init(&buf->list);
		vbuf->vb2_buf.timestamp = ktime_get_ns();
		vbuf->sequence = sidev->sequence++;
		vbuf->field = V4L2_FIELD_NONE;
		vb2_buffer_done(&vbuf->vb2_buf, VB2_BUF_STATE_DONE);
	}
	spin_unlock_irq(&sidev->lock);
	//end of test

	
	

	return 0;

err_reset:
	v4l2_subdev_call(sidev->entity.subdev, video, s_stream, 0);

	//pm_runtime_put(sidev->dev);

	spin_lock_irq(&sidev->lock);
	sidev->active = NULL;
	/* Release all active buffers */
	list_for_each_entry_safe(buf, node, &sidev->video_buffer_list, list) {
		list_del_init(&buf->list);
		vb2_buffer_done(&buf->vb.vb2_buf, VB2_BUF_STATE_QUEUED);
	}
	//INIT_LIST_HEAD(&sidev->video_buffer_list);
	spin_unlock_irq(&sidev->lock);

	return ret;
}


static void caninos_si_stop_streaming(struct vb2_queue *vq)
{
    struct caninos_si_dev *sidev = vb2_get_drv_priv(vq);
	struct frame_buffer *buf, *tmp;
	int ret = 0;
	//unsigned long timeout;
    unsigned long flag;
	buf = sidev->active;

	pr_info("%s: init", __func__);
	
	
	owl_camera_hw_call(sidev->hw_adapter, dump_isp_reg, sidev->channel);
	/* Wait until the end of the current frame */
	if (sidev->active && !wait_for_completion_timeout(&sidev->complete, 5 * HZ))
		v4l2_err(&sidev->v4l2_dev,
			 "Timeout waiting for end of the capture\n");

	/* Disable stream on the sub device */
	ret = v4l2_subdev_call(sidev->entity.subdev, video, s_stream, 0);
	if (ret && ret != -ENOIOCTLCMD)
		dev_err(sidev->dev, "stream off failed in subdev\n");

	spin_lock_irqsave(&sidev->lock, flag);
	if(sidev->active){
		/* Release all active buffers */
		list_for_each_entry_safe(buf, tmp,&sidev->video_buffer_list, list) {
			list_del_init(&buf->list);
			vb2_buffer_done(&buf->vb.vb2_buf, VB2_BUF_STATE_ERROR);
		}
	}
    sidev->active = NULL;

	//disable interrupts
	caninos_si_disable_all_int(sidev);
	//clear any pending
	caninos_si_clean_all_pending(sidev);

	//INIT_LIST_HEAD(&sidev->video_buffer_list);
	spin_unlock_irqrestore(&sidev->lock, flag);

	/* Stop all pending DMA operations */
	//mutex_lock(&sidev->dma_lock);
	//dmaengine_terminate_all(sidev->dma_chan);
	//mutex_unlock(&sidev->dma_lock);

    caninos_si_capture_stop(sidev);

	//pm_runtime_put(sidev->dev);

}


static struct vb2_ops caninos_videobuffer_ops = {
    .queue_setup = caninos_si_queue_setup,
    .buf_prepare = caninos_si_buffer_prepare,
    .buf_queue = caninos_si_buffer_queue,
    .buf_cleanup = caninos_si_buffer_cleanup,
    .buf_init = caninos_si_videobuf_init,
    .wait_prepare = vb2_ops_wait_prepare,
    .wait_finish = vb2_ops_wait_finish,
    .start_streaming = caninos_si_start_streaming,
    .stop_streaming = caninos_si_stop_streaming,
};

static int caninos_si_get_sensor_format(struct caninos_si_dev *sidev,
				  struct v4l2_pix_format *pix)
{
	struct v4l2_subdev_format fmt = {
		.which = V4L2_SUBDEV_FORMAT_ACTIVE,
	};
	int ret;

	ret = v4l2_subdev_call(sidev->entity.subdev, pad, get_fmt, NULL, &fmt);
	if (ret)
		return ret;

	v4l2_fill_pix_format(pix, &fmt.format);

	return 0;
}


static int caninos_si_get_sensor_bounds(struct caninos_si_dev *sidev,
				  struct v4l2_rect *r)
{
	struct v4l2_subdev_selection bounds = {
		.which = V4L2_SUBDEV_FORMAT_ACTIVE,
		.target = V4L2_SEL_TGT_CROP_BOUNDS,
	};
	struct v4l2_pix_format pix;
	int ret;
	/*
	 * If frame sizes enumeration is not implemented,
	 * fallback by getting current sensor frame size
	 */
	ret = caninos_si_get_sensor_format(sidev, &pix);
	if (ret)
		return ret;

	r->top = 0;
	r->left = 0;
	r->width = pix.width;
	r->height = pix.height;

	return 0;
}

static const struct si_format *find_format_by_fourcc(struct caninos_si_dev *sidev,
						      unsigned int fourcc)
{
	unsigned int num_formats = sidev->num_user_formats;
	const struct si_format *fmt;
	unsigned int i;

	pr_info("%s: init", __func__);

	for (i = 0; i < num_formats; i++) {
		fmt = sidev->user_formats[i];
		if (fmt->fourcc == fourcc)
			return fmt;
	}

	return NULL;
}

static int caninos_si_set_sensor_format(struct caninos_si_dev *sidev,
				  struct v4l2_pix_format *pix)
{
	const struct si_format *si_fmt;
	struct v4l2_subdev_format format = {
		.which = V4L2_SUBDEV_FORMAT_TRY,
	};
	struct v4l2_subdev_pad_config pad_cfg;
	int ret;

	si_fmt = find_format_by_fourcc(sidev, pix->pixelformat);
	if (!si_fmt) {
		if (!sidev->num_user_formats){
			pr_err("%s: no num_user_formats", __func__);
			return -ENODATA;
		}

		si_fmt = sidev->user_formats[sidev->num_user_formats - 1];
		pix->pixelformat = si_fmt->fourcc;
	}

	v4l2_fill_mbus_format(&format.format, pix, si_fmt->mbus_code);
	ret = v4l2_subdev_call(sidev->entity.subdev, pad, set_fmt,
			       &pad_cfg, &format);
	if (ret < 0)
		return ret;

	return 0;
}



/** 
 * using preline interrupt of current active buffer to indicate that previous buffer
 * have received all data from sensor, unlike old manner to indicate current buffer's.
 * so ISP_PRELINE_NUM should be as small as possible.
 * to do this taking tow reasons into account:
 * 1. no need to check or wait for frame end interrupt pending which is less useful for ISR.
 * 2. max time of  interrupt response delay shoud be less than about 30ms for 30 fps.
 *     if interval time between preline-interrupt and vsync is less than 30ms, 
 *     there is a risk that ISR receive tow preline interrupt between 
 *     tow adjacent vsync, a buffer will be skipped leaving old data in it.
 */


//FIXME (as soon as possible turn it off to test what happen, probably it can be removed, it make no sense at all)
//it just turn turn the GPIOC31 to input 
/*static int owl_camera_reset(void __iomem *reg_addr, int reset){
    
    if(reset == 1){
        writel(readl(reg_addr)|(0x1<<31),reg_addr);
    }else{
        writel(readl(reg_addr)&(~(0x1<<31)),reg_addr);
    }
    return 0;
}*/


static irqreturn_t caninos_si_isr(int irq, void *data)
{
	
    struct caninos_si_dev *sidev = data;
    int ret;
    unsigned long flags;
    unsigned int preline_int_pd,frameend_int_pd;
	unsigned int si_int_stat;
   // void __iomem *reg_addr;
    //struct vb2_buffer *tmp_vbbuffer = NULL;
	//ret = owl_camera_hw_call(hw_adapter, clear_channel_frameend_int_en,cam_param->cam_param->channel);
	pr_info("%s: init", __func__);
	preline_int_pd = sidev->hw_adapter->preline_int_pd[sidev->channel];
	frameend_int_pd = sidev->hw_adapter->frameend_int_pd[sidev->channel];

	//Interrupt for streaming start
	if (sidev->active) {
		struct vb2_v4l2_buffer *vbuf = &sidev->active->vb;
		struct frame_buffer *buf = sidev->active;

		list_del_init(&buf->list);
		vbuf->vb2_buf.timestamp = ktime_get_ns();
		vbuf->sequence = sidev->sequence++;
		vbuf->field = V4L2_FIELD_NONE;
		vb2_buffer_done(&vbuf->vb2_buf, VB2_BUF_STATE_DONE);
	}

	if (list_empty(&sidev->video_buffer_list)) {
		sidev->active = NULL;
	}else{
		sidev->active = list_entry(sidev->video_buffer_list.next,
					struct frame_buffer, list);
	}
	//Interrupt for streaming end	

	//Interrupts for device flags start
	spin_lock_irqsave(&sidev->lock, flags);
	si_int_stat = owl_camera_hw_call(sidev->hw_adapter, get_channel_state,sidev->channel);
	
	if(owl_camera_hw_call(sidev->hw_adapter, get_channel_overflow,sidev->channel))
	{
		pr_err("si is over flow , reset si\n");

		owl_camera_hw_call(sidev->hw_adapter, clear_channel_overflow, sidev->channel);
		ret |= owl_camera_hw_call(sidev->hw_adapter, clear_channel_en, sidev->channel);
		owl_camera_hw_call(sidev->hw_adapter, save_regs);

		reset_si_module();

		owl_camera_hw_call(sidev->hw_adapter, restore_regs);
		caninos_si_set_rect(sidev);
		caninos_si_start_dma(sidev, &sidev->active->vb.vb2_buf);
		ret |= owl_camera_hw_call(sidev->hw_adapter, set_channel_en, sidev->channel);
		goto out;
	}
	if (sidev->status == DEV_STOP && (si_int_stat & frameend_int_pd)) {
        pr_info("found stop in si_isr");
		//ret |= owl_camera_hw_call(sidev->hw_adapter, clear_channel_frameend_int_en, sidev->channel);
        complete(&sidev->complete);
        goto out;
    }
	//if(si_int_stat & preline_int_pd)
	//{
	//    ret = owl_camera_hw_call(sidev->hw_adapter, clear_channel_preline_pending,sidev->channel);
	//}
  //	if(si_int_stat & frameend_int_pd)
	//{
	//    ret = owl_camera_hw_call(sidev->hw_adapter, clear_channel_frameend_pending,sidev->channel);
	//}
	pr_info("channel:%d, isr S intstat 0x%08x,preline_int_pd  0x%08x", sidev->channel, si_int_stat,preline_int_pd);
    /*if (si_int_stat & preline_int_pd) {
        // send out a packet already recevied all data
        if (sidev->prev_frm != NULL) {
            frm_buff->vb.vb2_buf = *sidev->prev_frm;
            //do_gettimeofday(vb->timestamp);
            frm_buff->vb.vb2_buf.timestamp = ktime_get_ns();
            //vb->v4l2_buf.sequence = sidev->sequence++;
            frm_buff->vb.sequence = sidev->sequence++;
            vb2_buffer_done(&frm_buff->vb.vb2_buf, VB2_BUF_STATE_DONE);
        }

        if (!list_empty(&sidev->video_buffer_list)) {
			//sidev->active = sidev->cur_frm;
			//if(tmp_vbbuffer){
			//	sidev->cur_frm = tmp_vbbuffer;
			//}
			//else{
			//	sidev->cur_frm = next_vb2_buf(sidev);
			//}

            caninos_si_set_rect(sidev);
            caninos_si_start_dma(sidev, &sidev->active->vb.vb2_buf);

        } else {
            sidev->active = NULL;
            caninos_si_set_rect(sidev);
            caninos_si_start_dma(sidev, &sidev->active->vb.vb2_buf);
        }

    }*/
  out:
	caninos_si_clean_all_pending(sidev);
    spin_unlock_irqrestore(&sidev->lock, flags);

    return IRQ_HANDLED;
}

#define SENSOR0_CLK 24000000
//Enable clks and powergate
static int caninos_si_init_device(struct caninos_si_dev *sidev)
{
	//mipi_setting *mipi_cfg = module_info->mipi_cfg;	
    int err;
	pr_info("%s: init", __func__);
    //vce_si_powergate_enable(true);
    //if(err & PWR_VCE){
    //    pr_info("powergate enabled success!");
    //}
	err = owl_powergate_power_on(OWL_POWERGATE_VCE_BISP);
	if (err) {
		pr_err("owl powergate power-on error %d", err);
		return err;
	}
	//module_reset(MODULE_RST_BISP);
    //reset_control_assert(sidev->rst);
    //mdelay(2);
	//reset_control_deassert(sidev->rst);

    clk_prepare_enable(sidev->si_clk);
    //err = clk_enable(sidev->si_clk);
    //err = clk_prepare_enable(isp_clk);
    if (err) {
        pr_err("enable si clock failed %d", err);
        return err;
    }

	module_clk_enable(MODULE_CLK_BISP);
  
    err = clk_set_rate(sidev->si_clk, SI_WORK_CLOCK);
    if (err) {
        pr_err("set si clock rate failed %d", err);
        return err;
    }
    printk(KERN_INFO "si clock is %dM", (int) (clk_get_rate(sidev->si_clk) / 1000000));

	clk_prepare(sidev->ch_clk[0]);
    err = clk_enable(sidev->ch_clk[0]);
	if (err) {
        pr_err("enable ch_clk[0] clock failed %d", err);
        return err;
    }

	err = clk_set_rate(sidev->ch_clk[0], SENSOR0_CLK);
    if (err) {
        pr_err("set ch_clk clock rate failed %d", err);
        return err;
    }
    printk(KERN_INFO "senso0 clock is %dM", (int) (clk_get_rate(sidev->ch_clk[0]) / 1000000));
	// CSI CLOCK 
	reset_si_module();

	//clk_prepare(sidev->csi_clk);
    //module_clk_enable(MOD_ID_CSI);
	//err = clk_set_rate(sidev->csi_clk, 60000000);
    //if (err) {
    //        pr_err("set csi clock rate failed %d", err);
    //        return err;
    //}
	//printk(KERN_INFO "csi clock is %dM", (int) (clk_get_rate(sidev->csi_clk) / 1000000));
#ifdef MIPI
	if(mipi_cfg)
    {
        clk_prepare(sidev->csi_clk);
    //		act_writel((act_readl(0xb01600a0)|(0x1<<13)),0xb01600a0);
        module_clk_enable(MOD_ID_CSI);
        printk(KERN_INFO "CMU_DEVCLKEN0 : 0x%x\n",act_readl(0xb01600a0));
        //printk(KERN_INFO "0xb01600a0 : 0x%x\n",act_readl(0xb01600a0));
        err = clk_enable(sidev->csi_clk);
        //err = clk_prepare_enable(csi_clk);
        if (err) {
            pr_err("enable csi clock failed %d", err);
            return err;
        }
        //printk(KERN_INFO "the csi clock : %d,0xb01600a0 : 0x%x", mipi_cfg->csi_clk,act_readl(0xb01600a0));
        err = clk_set_rate(sidev->csi_clk, mipi_cfg->csi_clk);
        if (err) {
            pr_err("set csi clock rate failed %d", err);
            return err;
        }
        printk(KERN_INFO "csi clock is %dM", (int) (clk_get_rate(sidev->csi_clk) / 1000000));
    }
#endif

    sidev->status = DEV_OPEN;

	//enable frame and preline interrupts
	caninos_si_enable_all_int(sidev);

	pr_info("%s:dev open", __func__);
    //pm_runtime_get_sync(sidev->dev);
	//writel(0xf,noc_si_to_ddr);
    //pr_info("[add_device] isp attached to sensor %d", icd->devnum);

    return 0;
}

static int caninos_si_set_bus_param(struct caninos_si_dev *sidev)
{
    //pm_runtime_get_sync(sidev->dev);
	pr_info("%s: init", __func__);
    //this is only called when s_mbus_config is implemented in sensor
    //ret = v4l2_subdev_call(sidev->entity->subdev), video, s_mbus_config, &cfg);

    caninos_si_set_signal_polarity(sidev, sidev->cam_param->flags);
    caninos_si_set_input_fmt(sidev, sidev->cam_param->pixel_code);

    //pm_runtime_put(sidev->dev);

    return 0;
}

static int check_frame_range(u32 width, u32 height)
{
    /* limit to owl_camera hardware capabilities */
    return height < 1 || height > SI_MAX_HEIGHT || width < 32
        || width > SI_MAX_WIDTH /* || (width % 32) */ ;
}

static int caninos_si_try_fmt(struct caninos_si_dev *sidev, struct v4l2_format *f, 
                              const struct si_format **current_fmt)
{
    const struct si_format *si_fmt;
    struct v4l2_pix_format *pix = &f->fmt.pix;
    struct v4l2_subdev_pad_config pad_cfg;
    struct v4l2_subdev_format format = {
		.which = V4L2_SUBDEV_FORMAT_TRY,
	};
    struct v4l2_mbus_framefmt *mf = &format.format;
	bool do_crop;
	int ret;

	pr_info("%s: init", __func__);
    si_fmt = find_format_by_fourcc(sidev, pix->pixelformat);

    if (!si_fmt) {
		si_fmt = sidev->user_formats[sidev->num_user_formats - 1];
		pix->pixelformat = si_fmt->fourcc;
	}

    if (pix->pixelformat && !si_fmt) {
        pr_err("format %x not found", pix->pixelformat);
        return -EINVAL;
    }

    /*
     * Limit to k5 CAMERA hardware capabilities.  YUV422P planar format requires
     * images size to be a multiple of 16 bytes.  If not, zeros will be
     * inserted between Y and U planes, and U and V planes, which violates
     * the YUV422P standard.
     */
    v4l_bound_align_image(&pix->width, 32, SI_MAX_WIDTH, 5,
                          &pix->height, 1, SI_MAX_HEIGHT, 0,
                          pix->pixelformat == V4L2_PIX_FMT_YUV422P ? 4 : 0);

    /* Limit to Actions SI hardware capabilities */
	pix->width = clamp(pix->width, 0U, SI_MAX_WIDTH);
	pix->height = clamp(pix->height, 0U, SI_MAX_HEIGHT);

	/* No crop if JPEG is requested */
	//do_crop = sidev->do_crop && (pix->pixelformat != V4L2_PIX_FMT_JPEG);


	v4l2_fill_mbus_format(mf, pix, si_fmt->mbus_code);
	//ret = v4l2_subdev_call(sidev->entity.subdev, pad, set_fmt,
	//		       &pad_cfg, &format);
	//if (ret < 0)
	//	return ret;

	v4l2_fill_pix_format(pix, &format.format);

	/*if (do_crop) {
		pr_info("doing crop");
		struct v4l2_rect c = sidev->crop;
		struct v4l2_rect max_rect;

		//
		 // Adjust crop by making the intersection between
		 // format resolution request and crop request
		 //
		max_rect.top = 0;
		max_rect.left = 0;
		max_rect.width = pix->width;
		max_rect.height = pix->height;
		v4l2_rect_map_inside(&c, &max_rect);
		c.top  = clamp_t(s32, c.top, 0, pix->height - c.height);
		c.left = clamp_t(s32, c.left, 0, pix->width - c.width);
		//sidev->crop = c;

		//Adjust format resolution request to crop 
		pix->width = sidev->crop.width;
		pix->height = sidev->crop.height;
	}
	*/
    pix->field = V4L2_FIELD_NONE;
    pix->bytesperline = pix->width * si_fmt->bpp;
    pix->sizeimage = pix->height * pix->bytesperline;
    
    if (pix->bytesperline < 0) {
        pr_err("bytesperline %d not correct.", pix->bytesperline);
        return pix->bytesperline;
    }
    

    /* limit to sensor capabilities */
    mf->width = pix->width;
    mf->height = pix->height;
    mf->field = pix->field;
    mf->colorspace = pix->colorspace;
    mf->code = si_fmt->mbus_code;

    pr_info("%s:try %dx%d ColorSpace = 0x%x", __func__,mf->width, mf->height, mf->colorspace);

    //i don't know if this is necessary here
    ret = v4l2_subdev_call(sidev->entity.subdev, pad, set_fmt, &pad_cfg, &format);
    if (ret < 0) {
        return ret;
    }

    if (current_fmt)
		*current_fmt = si_fmt;

    return 0;
}

static int caninos_si_set_fmt(struct caninos_si_dev *sidev, struct v4l2_format *f)
{

    struct v4l2_subdev_format format = {
		.which = V4L2_SUBDEV_FORMAT_ACTIVE,
	};
	const struct si_format *current_fmt;
    struct v4l2_subdev *sd = sidev->entity.subdev;
    struct v4l2_pix_format *pix = &f->fmt.pix;
    struct v4l2_mbus_framefmt *mf = &format.format;
	struct v4l2_subdev_pad_config pad_cfg;
    unsigned int skip_frames_num;
    int ret;

	pr_info("%s: init",__func__);

	caninos_si_set_bus_param(sidev);

    ret = caninos_si_try_fmt(sidev, f, &current_fmt);
	if (ret){
		pr_info("%s: fail to try_fmt",__func__);
		return ret;
	}

	/* Disable crop if JPEG is requested */
	//if (pix->pixelformat == V4L2_PIX_FMT_JPEG)
	//	sidev->do_crop = false;
    //pr_info("%s, S_FMT(pix=0x%x, %ux%u)", __func__, pixfmt, pix->width, pix->height);

    /* sensor may skip different some frames */
    //ret = v4l2_subdev_call(sd, sensor, g_skip_frames, &skip_frames_num);
    //if (ret < 0) {
    //    skip_frames_num = 0;
	//	pr_info("%s: fail to get skip_frames, so is 0",__func__);
    //}
    //sidev->cam_param->skip_frames = skip_frames_num;

    ret = v4l2_subdev_call(sd, pad, set_fmt, &pad_cfg, &format);
    if (ret < 0) {
        pr_err("failed to configure for format %x", pix->pixelformat);
        return ret;
    }else{
		pr_info("%s: ov5640 set_fmt ok", __func__);
	}

    if (check_frame_range(mf->width, mf->height)) {
        pr_err("sensor produced an unsupported frame %dx%d", mf->width, mf->height);
        return -EINVAL;
    }

    sidev->cam_param->isp_left = 0;
    sidev->cam_param->isp_top = 0;
    sidev->cam_param->width = mf->width;
    sidev->cam_param->height = mf->height;
    sidev->cam_param->pixel_code = current_fmt->mbus_code;


	sidev->fmt = *f;
    sidev->current_fmt = current_fmt;
    //sidev->user_width = cam_param->width;
    //sidev->user_height = cam_param->height;

    pr_info("sensor set %dx%d, code = %#x", pix->width, pix->height, sidev->cam_param->pixel_code);

    caninos_si_set_rect(sidev);

    ret = caninos_si_set_output_fmt(sidev, sidev->current_fmt->fourcc);
    if(ret)
        return ret;

    //pr_info("set output data format %s", icd->current_fmt->host_fmt->name);

    return 0;
}

static int caninos_si_querycap(struct file *file, void *priv,
			                    struct v4l2_capability *cap)
{
    /* cap->name is set by the firendly caller:-> */
    //strlcpy(cap->card, "OWL Camera", sizeof(cap->card));
    //cap->version = KERNEL_VERSION(0, 0, 1);
    //cap->capabilities = V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_STREAMING;
	pr_info("%s: init", __func__);
    strscpy(cap->driver, "caninos-si", sizeof(cap->driver));
	strscpy(cap->card, "Caninos Sensor Interface", sizeof(cap->card));
	strscpy(cap->bus_info, "platform:caninos-si", sizeof(cap->bus_info));

    return 0;
}

static int caninos_si_try_fmt_vid_cap(struct file *file, void *priv,
				struct v4l2_format *f)
{
	struct caninos_si_dev *sidev = video_drvdata(file);
	pr_info("%s: init", __func__);
	return caninos_si_try_fmt(sidev, f, NULL);
}

static int caninos_si_g_fmt_vid_cap(struct file *file, void *priv,
			      struct v4l2_format *fmt)
{
	struct caninos_si_dev *sidev = video_drvdata(file);

	pr_info("%s: init", __func__);
	*fmt = sidev->fmt; //FIXME 
    // i beleive that here i should use subdev_ops get_fmt

	return 0;
}

static int caninos_si_s_fmt_vid_cap(struct file *file, void *priv,
			      struct v4l2_format *f)
{
	struct caninos_si_dev *sidev = video_drvdata(file);

	pr_info("%s: init", __func__);
	if (vb2_is_streaming(&sidev->queue))
		return -EBUSY;

	return caninos_si_set_fmt(sidev, f);
}

static int caninos_si_enum_fmt_vid_cap(struct file *file, void  *priv,
				struct v4l2_fmtdesc *f)
{
	struct caninos_si_dev *sidev = video_drvdata(file);

	pr_info("%s: init", __func__);
	if (f->index >= sidev->num_user_formats)
		return -EINVAL;

	f->pixelformat = sidev->user_formats[f->index]->fourcc;
	return 0;
}

static int caninos_si_enum_input(struct file *file, void *priv,
			   struct v4l2_input *i)
{
	pr_info("%s: init", __func__);
	if (i->index != 0)
		return -EINVAL;

	i->type = V4L2_INPUT_TYPE_CAMERA;
	strscpy(i->name, "Camera", sizeof(i->name));
	return 0;
}

static int caninos_si_g_selection(struct file *file, void *fh,
			    struct v4l2_selection *s)
{
	struct caninos_si_dev *sidev = video_drvdata(file);

	pr_info("%s: init", __func__);
	if (s->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		return -EINVAL;

	switch (s->target) {
	case V4L2_SEL_TGT_CROP_DEFAULT:
	case V4L2_SEL_TGT_CROP_BOUNDS:
		s->r = sidev->bounds;
		return 0;
	case V4L2_SEL_TGT_CROP:
		if (sidev->do_crop) {
			s->r = sidev->crop;
		} else {
			s->r.top = 0;
			s->r.left = 0;
			s->r.width = sidev->fmt.fmt.pix.width;
			s->r.height = sidev->fmt.fmt.pix.height;
		}
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static int caninos_si_s_selection(struct file *file, void *priv,
			    struct v4l2_selection *s)
{
	struct caninos_si_dev *sidev = video_drvdata(file);
	struct v4l2_rect r = s->r;
	struct v4l2_rect max_rect;
	struct v4l2_pix_format pix;

	if (s->type != V4L2_BUF_TYPE_VIDEO_CAPTURE ||
	    s->target != V4L2_SEL_TGT_CROP)
		return -EINVAL;

	/* Reset sensor resolution to max resolution */
	pix.pixelformat = sidev->fmt.fmt.pix.pixelformat;
	pix.width = sidev->bounds.width;
	pix.height = sidev->bounds.height;
	caninos_si_set_sensor_format(sidev, &pix);

	/*
	 * Make the intersection between
	 * sensor resolution
	 * and crop request
	 */
	max_rect.top = 0;
	max_rect.left = 0;
	max_rect.width = pix.width;
	max_rect.height = pix.height;
	v4l2_rect_map_inside(&r, &max_rect);
	r.top  = clamp_t(s32, r.top, 0, pix.height - r.height);
	r.left = clamp_t(s32, r.left, 0, pix.width - r.width);

	/*/if (!(r.top == sidev->bounds.top &&
	      r.left == sidev->bounds.left &&
	      r.width == sidev->bounds.width &&
	      r.height == sidev->bounds.height)) {
		//Crop if request is different than sensor resolution 
		sidev->do_crop = true;
		sidev->crop = r;
		dev_info(sidev->dev, "s_selection: crop %ux%u@(%u,%u) from %ux%u\n",
			r.width, r.height, r.left, r.top,
			pix.width, pix.height);
	} else {
		/* Disable crop 
		sidev->do_crop = false;
		dev_info(sidev->dev, "s_selection: crop is disabled\n");
	}*/

	s->r = r;
	return 0;
}

static int caninos_si_g_input(struct file *file, void *priv, unsigned int *i)
{
	*i = 0;
	return 0;
}


static int caninos_si_s_input(struct file *file, void *priv, unsigned int i)
{
	pr_info("%s: init", __func__);
	if (i > 0)
		return -EINVAL;
	return 0;
}

static int caninos_si_g_parm(struct file *file, void *fh, struct v4l2_streamparm *a)
{
	struct caninos_si_dev *sidev = video_drvdata(file);
	pr_info("%s: init", __func__);
	return v4l2_g_parm_cap(video_devdata(file), sidev->entity.subdev, a);
}

static int caninos_si_s_parm(struct file *file, void *fh, struct v4l2_streamparm *a)
{
	struct caninos_si_dev *sidev = video_drvdata(file);
	pr_info("%s: init", __func__);
	return v4l2_s_parm_cap(video_devdata(file), sidev->entity.subdev, a);
}

static int caninos_si_enum_framesizes(struct file *file, void *fh,
			       struct v4l2_frmsizeenum *fsize)
{
	struct caninos_si_dev *sidev = video_drvdata(file);
	const struct si_format *si_fmt;
	struct v4l2_subdev_frame_size_enum fse = {
		.index = fsize->index,
		.which = V4L2_SUBDEV_FORMAT_ACTIVE,
	};
	int ret;

	pr_info("%s: init", __func__);

	si_fmt = find_format_by_fourcc(sidev, fsize->pixel_format);
	if (!si_fmt)
		return -EINVAL;

	fse.code = si_fmt->mbus_code;

	ret = v4l2_subdev_call(sidev->entity.subdev, pad, enum_frame_size,
			       NULL, &fse);
	if (ret)
		return ret;

	fsize->type = V4L2_FRMSIZE_TYPE_DISCRETE;
	fsize->discrete.width = fse.max_width;
	fsize->discrete.height = fse.max_height;

	pr_info("%s: frame size: width: %d, height: %d", __func__,
	fsize->discrete.width, fsize->discrete.height);

	return 0;
}

static int caninos_si_enum_frameintervals(struct file *file, void *fh,
				    struct v4l2_frmivalenum *fival)
{
	struct caninos_si_dev *sidev = video_drvdata(file);
	const struct si_format *si_fmt;
	struct v4l2_subdev_frame_interval_enum fie = {
		.index = fival->index,
		.width = fival->width,
		.height = fival->height,
		.which = V4L2_SUBDEV_FORMAT_ACTIVE,
	};
	int ret;

	pr_info("%s: init", __func__);

	si_fmt = find_format_by_fourcc(sidev, fival->pixel_format);
	if (!si_fmt){
		pr_info("%s: fail to get si_fmt", __func__);
		return -EINVAL;
	}

	fie.code = si_fmt->mbus_code;

	ret = v4l2_subdev_call(sidev->entity.subdev, pad,
			       enum_frame_interval, NULL, &fie);
	if (ret){
		pr_info("%s: fail on enum_frame_interval", __func__);
		return ret;
	}

	fival->type = V4L2_FRMIVAL_TYPE_DISCRETE;
	fival->discrete = fie.interval;

	return 0;
}

static const struct v4l2_ioctl_ops caninos_si_ioctl_ops = {
	.vidioc_querycap		= caninos_si_querycap,

	.vidioc_try_fmt_vid_cap		= caninos_si_try_fmt_vid_cap,
	.vidioc_g_fmt_vid_cap		= caninos_si_g_fmt_vid_cap,
	.vidioc_s_fmt_vid_cap		= caninos_si_s_fmt_vid_cap,
	.vidioc_enum_fmt_vid_cap	= caninos_si_enum_fmt_vid_cap,
	//.vidioc_g_selection		= caninos_si_g_selection,
	//.vidioc_s_selection		= caninos_si_s_selection,

	.vidioc_enum_input		= caninos_si_enum_input,
	.vidioc_g_input			= caninos_si_g_input,
	.vidioc_s_input			= caninos_si_s_input,

	.vidioc_g_parm			= caninos_si_g_parm,
	.vidioc_s_parm			= caninos_si_s_parm,
	.vidioc_enum_framesizes		= caninos_si_enum_framesizes,
	.vidioc_enum_frameintervals	= caninos_si_enum_frameintervals,

	.vidioc_reqbufs			= vb2_ioctl_reqbufs,
	.vidioc_create_bufs		= vb2_ioctl_create_bufs,
	.vidioc_querybuf		= vb2_ioctl_querybuf,
	.vidioc_qbuf			= vb2_ioctl_qbuf,
	.vidioc_dqbuf			= vb2_ioctl_dqbuf,
	.vidioc_expbuf			= vb2_ioctl_expbuf,
	.vidioc_prepare_buf		= vb2_ioctl_prepare_buf,
	.vidioc_streamon		= vb2_ioctl_streamon,
	.vidioc_streamoff		= vb2_ioctl_streamoff,

	.vidioc_log_status		= v4l2_ctrl_log_status,
	.vidioc_subscribe_event		= v4l2_ctrl_subscribe_event,
	.vidioc_unsubscribe_event	= v4l2_event_unsubscribe,
};

static int caninos_si_open(struct file *file)
{
	struct caninos_si_dev *sidev = video_drvdata(file);
	struct v4l2_subdev *sd = sidev->entity.subdev;
	int ret;

	pr_info("%s: init", __func__);

	if (mutex_lock_interruptible(&sidev->mutex_lock)){
		pr_err("mutex fail on lock during open");
		return -ERESTARTSYS;
	}

	ret = v4l2_fh_open(file);
	if (ret < 0)
		goto unlock;

	if (!v4l2_fh_is_singular_file(file))
		goto fh_rel;

	ret = v4l2_subdev_call(sd, core, s_power, 1);
	if (ret < 0 && ret != -ENOIOCTLCMD)
		goto fh_rel;

	ret = caninos_si_set_fmt(sidev, &sidev->fmt);
	if (ret)
		v4l2_subdev_call(sd, core, s_power, 0);
fh_rel:
	if (ret)
		v4l2_fh_release(file);
unlock:
	mutex_unlock(&sidev->mutex_lock);
	return ret;
}

static int caninos_si_release(struct file *file)
{
	struct caninos_si_dev *sidev = video_drvdata(file);
	struct v4l2_subdev *sd = sidev->entity.subdev;
	bool fh_singular;
	int ret;

	pr_info("%s: init", __func__);

	mutex_lock(&sidev->mutex_lock);

	fh_singular = v4l2_fh_is_singular_file(file);

	ret = _vb2_fop_release(file, NULL);

	if (fh_singular)
		v4l2_subdev_call(sd, core, s_power, 0);

	mutex_unlock(&sidev->mutex_lock);

	return ret;
}

//mapping to userspace
static int caninos_si_video_mmap(struct file *file, struct vm_area_struct *vma)
{
	//struct isp_video_fh *vfh = to_isp_video_fh(file->private_data);
	struct video_device *vdev = video_devdata(file);
	pr_info("mmap memory at start address 0x%x to end addr 0x%x", vma->vm_start, vma->vm_end);
	return vb2_mmap(vdev->queue, vma);
}


static const struct v4l2_file_operations caninos_si_fops = {
	.owner		= THIS_MODULE,
	.unlocked_ioctl	= video_ioctl2,
	.open		= caninos_si_open,
	.release	= caninos_si_release,
	.poll		= vb2_fop_poll,
	.mmap		= caninos_si_video_mmap,
	.read		= vb2_fop_read,
};

static int caninos_si_get_clock(struct device *dev, struct caninos_si_dev *sidev)
{
    struct clk *ret;
	struct clk *csi_parent_clk = NULL;
    int err;

	pr_info("%s: init", __func__);

#ifdef MCU_K7
    ret = devm_clk_get(dev, "si_clk");
#else
    ret = clk_get(NULL, CLKNAME_BISP_CLK);
#endif
    if (IS_ERR(ret)) {
        err = PTR_ERR(ret);
        pr_err("get isp clock error%d", err);
        goto si_clk_err;
    }
    sidev->si_clk = ret;

#ifdef MCU_K7
    ret = devm_clk_get(dev, "csi_clk");
#else
    ret = clk_get(NULL, CLKNAME_CSI_CLK);
#endif
    if (IS_ERR(ret)) {
        err = PTR_ERR(ret);
        pr_err("get csi clock error%d", err);
        goto csi_err;
    }
    sidev->csi_clk = ret;

#ifdef MCU_K7
    csi_parent_clk = NULL;
#else
    csi_parent_clk = clk_get(NULL, CLKNAME_DEV_CLK);
    //csi_parent_clk = devm_clk_get(dev, "dev_clk");
	if (IS_ERR(csi_parent_clk)) {
        err = PTR_ERR(csi_parent_clk);
        pr_err("get csi_parent_clk clock error%d", err);
    }
	clk_set_parent(sidev->csi_clk, csi_parent_clk);
#endif

#ifdef MCU_K7
    ret = devm_clk_get(dev, "sensor_ch0_clk");
#else
    ret = clk_get(NULL, CLKNAME_SENSOR_CLKOUT0);
#endif
    if (IS_ERR(ret)) {
        err = PTR_ERR(ret);
        pr_err("get isp-channel-0 clock error%d", err);
        goto ch0_err;
    }
    sidev->ch_clk[SI_CHANNEL_0] = ret;

#ifdef MCU_K7
    ret = devm_clk_get(dev, "sensor_ch1_clk");
#else
    ret = clk_get(NULL, CLKNAME_SENSOR_CLKOUT1);
#endif
    if (IS_ERR(ret)) {
        err = PTR_ERR(ret);
        pr_err("get isp-channel-1 clock error%d", err);
        goto ch1_err;
    }
    sidev->ch_clk[SI_CHANNEL_1] = ret;

    return 0; 

  ch1_err:
    clk_put(sidev->ch_clk[SI_CHANNEL_1]);
  ch0_err:
    clk_put(sidev->ch_clk[SI_CHANNEL_0]);
  csi_err:
    clk_put(sidev->csi_clk);
  si_clk_err:
    clk_put(sidev->si_clk);
    return err;
}

static int caninos_si_parse_dt(struct caninos_si_dev *sidev,
			struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	struct v4l2_fwnode_endpoint ep = { .bus_type = 0 };
	int err;

	pr_info("%s: init", __func__);

	//Here i will try se some gpios to know if one of then is enable or reset
	sidev->power_gpio = of_get_named_gpio(pdev->dev.of_node, "power-gpio", 0);
	if (!gpio_is_valid(sidev->power_gpio)) {
		pr_err("could not find power-gpio");
		return -ENODEV;
	}
	err = devm_gpio_request(&pdev->dev, sidev->power_gpio, "power-gpio");
	if(err){
		pr_err("could not request power-gpio");
		return err;
	}

	sidev->reset_gpio = of_get_named_gpio(pdev->dev.of_node, "reset-gpio", 0);
	if (!gpio_is_valid(sidev->reset_gpio)) {
		pr_err("could not find reset-gpio");
		return -ENODEV;
	}
	err = devm_gpio_request(&pdev->dev, sidev->reset_gpio, "reset-gpio");
	if(err){
		pr_err("could not request reset-gpio");
		return err;
	}

	sidev->standby_gpio = of_get_named_gpio(pdev->dev.of_node, "standby-gpio", 0);
	if (!gpio_is_valid(sidev->standby_gpio)) {
		pr_err("could not find standby-gpio");
		return -ENODEV;
	}
	err = devm_gpio_request(&pdev->dev, sidev->standby_gpio, "standby-gpio");
	if(err){
		pr_err("could not request standby-gpio");
		return err;
	}
	
	gpio_direction_output(sidev->power_gpio, 0);
	gpio_direction_output(sidev->reset_gpio, 0);
	gpio_direction_output(sidev->standby_gpio, 0);
	//now try gpio C7 and C9 high and D28 low

	//power the device up
	gpio_set_value(sidev->power_gpio, 0);
	mdelay(5);
	gpio_set_value(sidev->power_gpio, 1);
	gpio_set_value(sidev->standby_gpio, 0);
	mdelay(5);
	//doing camera reset
	gpio_set_value(sidev->reset_gpio, 0);
	mdelay(25);
	gpio_set_value(sidev->reset_gpio, 1);
	mdelay(25);

	np = of_graph_get_next_endpoint(np, NULL);
	if (!np) {
		dev_err(&pdev->dev, "Could not find the endpoint\n");
		return -EINVAL;
	}else{
		pr_info("endpoint found");
	}

	err = v4l2_fwnode_endpoint_parse(of_fwnode_handle(np), &ep);
	of_node_put(np);
	if (err) {
		dev_err(&pdev->dev, "Could not parse the endpoint\n");
		return err;
	}else{
		pr_info("parse endpoint ok");
	}

	switch (ep.bus.parallel.bus_width) {
	case 8:
		sidev->pdata.data_width_flags = SI_DATAWIDTH_8;
		break;
	case 10:
		sidev->pdata.data_width_flags =
				SI_DATAWIDTH_8 | SI_DATAWIDTH_10;
		break;
	default:
		dev_err(&pdev->dev, "Unsupported bus width: %d\n",
				ep.bus.parallel.bus_width);
		return -EINVAL;
	}

	/*if (ep.bus.parallel.flags & V4L2_MBUS_HSYNC_ACTIVE_LOW)
		sidev->pdata.hsync_act_low = true;
	if (ep.bus.parallel.flags & V4L2_MBUS_VSYNC_ACTIVE_LOW)
		sidev->pdata.vsync_act_low = true;
	if (ep.bus.parallel.flags & V4L2_MBUS_PCLK_SAMPLE_FALLING)
		sidev->pdata.pclk_act_falling = true;

	if (ep.bus_type == V4L2_MBUS_BT656)
		sidev->pdata.has_emb_sync = true;
    */
    sidev->cam_param->flags = ep.bus.parallel.flags;

	if(of_property_read_u32(np, "channel", sidev->channel)){
		pr_err("Fail to parse SI channel, using the default:Sensor_ch0");
		sidev->channel = 0;
	}else{
		pr_info("caninos-SI: using SI-channel_%d",sidev->channel);
	}

	if(of_property_read_u32(np, "si-channel_ds", sidev->ds)){
		pr_info("Fail to parse SI channel ds, using the default:Sensor_ch_ds0");
		sidev->ds = 0;
	}else{
		pr_info("caninos-SI: using SI-channel%d_ds%d",sidev->channel, sidev->ds);
	}

	return 0;
}

static const struct si_format caninos_si_formats[] = {
    {
        .fourcc = V4L2_PIX_FMT_YUV420,
        //.name = "YUV 4:2:0 planar 12 bit",
        .mbus_code = MEDIA_BUS_FMT_UYVY8_2X8,
        .bpp = 2,
        //.bits_per_sample = 12,  
    },
	{   .fourcc = V4L2_PIX_FMT_YVU420, 
		//.name = "YVU 4:2:0 planar 12 bit",        
		//.bits_per_sample = 12,        
		.mbus_code = MEDIA_BUS_FMT_UYVY8_2X8,
        .bpp = 2,
    },
    {
        .fourcc = V4L2_PIX_FMT_YUV422P,
        //.name = "YUV 4:2:2 planar 16 bit",
        //.bits_per_sample = 16,
        .mbus_code = MEDIA_BUS_FMT_UYVY8_2X8,
        .bpp = 2,
        
    },
    {
        .fourcc = V4L2_PIX_FMT_YUYV,
        //.name = "YUYV 4:2:2 interleaved 16bit",
        //.bits_per_sample = 16,
        .mbus_code = MEDIA_BUS_FMT_UYVY8_2X8,
        .bpp = 2,
    },
    {
        .fourcc = V4L2_PIX_FMT_NV12,
        //.name = "YUV 4:2:0 semi-planar 12 bit",
        //.bits_per_sample = 12,
        .mbus_code = MEDIA_BUS_FMT_UYVY8_2X8,
        .bpp = 2,
    },
{       .fourcc = V4L2_PIX_FMT_NV21,        
		//.name = "YUV 4:2:0 semi-planar 12 bit",       
		 //.bits_per_sample = 12,        
		.mbus_code = MEDIA_BUS_FMT_UYVY8_2X8,
        .bpp = 2,
    },  
};

static int caninos_si_formats_init(struct caninos_si_dev *sidev)
{
	const struct si_format *si_fmts[ARRAY_SIZE(caninos_si_formats)];
	unsigned int num_fmts = 0, i, j;
	struct v4l2_subdev *subdev = sidev->entity.subdev;
	struct v4l2_subdev_mbus_code_enum mbus_code = {
		.which = V4L2_SUBDEV_FORMAT_ACTIVE,
	};

	pr_info("%s: init", __func__);

	while (!v4l2_subdev_call(subdev, pad, enum_mbus_code,
				 NULL, &mbus_code)) {
		for (i = 0; i < ARRAY_SIZE(caninos_si_formats); i++) {
			if (caninos_si_formats[i].mbus_code != mbus_code.code)
				continue;

			/* Code supported, have we got this fourcc yet? */
			for (j = 0; j < num_fmts; j++)
				if (si_fmts[j]->fourcc == caninos_si_formats[i].fourcc)
					/* Already available */
					break;
			if (j == num_fmts)
				/* new */
				si_fmts[num_fmts++] = caninos_si_formats + i;
		}
		mbus_code.index++;
	}

	if (!num_fmts){
		pr_info("could no find formats");
		return -ENXIO;
	}else{
		pr_info("%s: num_user_formats = %d", __func__, num_fmts);
	}

	sidev->num_user_formats = num_fmts;
	sidev->user_formats = devm_kcalloc(sidev->dev,
					 num_fmts, sizeof(struct sidev_format *),
					 GFP_KERNEL);
	if (!sidev->user_formats)
		return -ENOMEM;

	memcpy(sidev->user_formats, si_fmts,
	       num_fmts * sizeof(struct si_format *));
	sidev->current_fmt = sidev->user_formats[0];

	return 0;
}

static int caninos_si_set_default_fmt(struct caninos_si_dev *sidev)
{
	struct v4l2_format f = {
		.type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
		.fmt.pix = {
			.width		= VGA_WIDTH,
			.height		= VGA_HEIGHT,
			.field		= V4L2_FIELD_NONE,
			.pixelformat	= sidev->user_formats[0]->fourcc,
		},
	};
	int ret;

	ret = caninos_si_try_fmt(sidev, &f, NULL);
	if (ret)
		return ret;
	sidev->current_fmt = sidev->user_formats[0];
	sidev->fmt = f;
	return 0;
}

static struct media_entity *caninos_si_find_source(struct caninos_si_dev *sidev)
{
	struct media_entity *entity = &sidev->vdev->entity;
	struct media_pad *pad;
	pr_info("%s: init", __func__);
	/* Walk searching for entity having no sink */
	while (1) {
		pad = &entity->pads[0];
		if (!(pad->flags & MEDIA_PAD_FL_SINK))
			break;

		pad = media_entity_remote_pad(pad);
		if (!pad || !is_media_entity_v4l2_subdev(pad->entity))
			break;

		entity = pad->entity;
	}

	return entity;
}

static int caninos_si_graph_notify_complete(struct v4l2_async_notifier *notifier)
{
	struct caninos_si_dev *sidev = container_of(notifier, struct caninos_si_dev, notifier);
	int ret;
	
	/*
	 * Now that the graph is complete,
	 * we search for the source subdevice
	 * in order to expose it through V4L2 interface
	 */
	sidev->entity.subdev =
		media_entity_to_v4l2_subdev(caninos_si_find_source(sidev));
	if (!sidev->entity.subdev) {
		dev_err(sidev->dev, "Source subdevice not found\n");
		return -ENODEV;
	}

	sidev->vdev->ctrl_handler = sidev->entity.subdev->ctrl_handler;

	ret = caninos_si_formats_init(sidev);
	if (ret) {
		dev_err(sidev->dev, "No supported mediabus format found\n");
		return ret;
	}

	caninos_si_set_bus_param(sidev);

	ret = caninos_si_get_sensor_bounds(sidev, &sidev->bounds);
	if (ret) {
		dev_err(sidev->dev, "Could not get sensor bounds\n");
		return ret;
	}

	ret = caninos_si_set_default_fmt(sidev);
	if (ret) {
		dev_err(sidev->dev, "Could not set default format\n");
		return ret;
	}
	return 0;
}

static void caninos_si_graph_notify_unbind(struct v4l2_async_notifier *notifier,
				     struct v4l2_subdev *sd,
				     struct v4l2_async_subdev *asd)
{
	struct caninos_si_dev *sidev = container_of(notifier, struct caninos_si_dev, notifier);

	pr_info("%s: init", __func__);
	dev_dbg(sidev->dev, "Removing %s\n", video_device_node_name(sidev->vdev));

	/* Checks internally if vdev have been init or not */
	video_unregister_device(sidev->vdev);
}

static int caninos_si_graph_notify_bound(struct v4l2_async_notifier *notifier,
				   struct v4l2_subdev *subdev,
				   struct v4l2_async_subdev *asd)
{
	struct caninos_si_dev *sidev = container_of(notifier, struct caninos_si_dev, notifier);
	unsigned int ret;
	int src_pad;
	pr_info("%s: init", __func__);
	dev_dbg(sidev->dev, "subdev %s bound\n", subdev->name);

	sidev->entity.subdev = subdev;
	/*
	 * Link this sub-device to caninos si, it could be
	 * a parallel camera sensor or a bridge
	 */
	src_pad = media_entity_get_fwnode_pad(&subdev->entity,
					      subdev->fwnode,
					      MEDIA_PAD_FL_SOURCE);

	ret = media_create_pad_link(&subdev->entity, src_pad,
				    &sidev->vdev->entity, 0,
				    MEDIA_LNK_FL_IMMUTABLE |
				    MEDIA_LNK_FL_ENABLED);
	if (ret)
		dev_err(sidev->dev, "Failed to create media pad link with subdev \"%s\"\n",
			subdev->name);
	else
		dev_info(sidev->dev, "Caninos SI is now linked to \"%s\"\n",
			subdev->name);

	return 0;
}


static const struct v4l2_async_notifier_operations caninos_si_graph_notify_ops = {
	.bound = caninos_si_graph_notify_bound,
	.unbind = caninos_si_graph_notify_unbind,
	.complete = caninos_si_graph_notify_complete,
};

static int caninos_si_graph_parse(struct caninos_si_dev *sidev, struct device_node *node)
{
	struct device_node *ep = NULL;
	struct device_node *remote;

	pr_info("%s: init", __func__);
	ep = of_graph_get_next_endpoint(node, ep);
	if (!ep)
		return -EINVAL;

	remote = of_graph_get_remote_port_parent(ep);
	of_node_put(ep);
	if (!remote)
		return -EINVAL;

	/* Remote node to connect */
	sidev->entity.node = remote;
	sidev->entity.asd.match_type = V4L2_ASYNC_MATCH_FWNODE;
	sidev->entity.asd.match.fwnode = of_fwnode_handle(remote);
	return 0;
}

static int caninos_si_graph_init(struct caninos_si_dev *sidev)
{
	int ret;
	pr_info("%s: init", __func__);
	/* Parse the graph to extract a list of subdevice DT nodes. */
	ret = caninos_si_graph_parse(sidev, sidev->dev->of_node);
	if (ret < 0) {
		dev_err(sidev->dev, "Graph parsing failed\n");
		return ret;
	}

	v4l2_async_notifier_init(&sidev->notifier);

	ret = v4l2_async_notifier_add_subdev(&sidev->notifier, &sidev->entity.asd);
	if (ret) {
		of_node_put(sidev->entity.node);
		return ret;
	}

	sidev->notifier.ops = &caninos_si_graph_notify_ops;

	ret = v4l2_async_notifier_register(&sidev->v4l2_dev, &sidev->notifier);
	if (ret < 0) {
		dev_err(sidev->dev, "Notifier registration failed\n");
		v4l2_async_notifier_cleanup(&sidev->notifier);
		return ret;
	}

	return 0;
}

#define CMU_BASE		0xB0160000
#define CMU_DEVCLKEN0	0xA0 + CMU_BASE
#define CMU_DEVCLKEN1   0xA4 + CMU_BASE
#define CMU_DEVRST0		0xA8 + CMU_BASE
#define CMU_DEVRST1		0xAC + CMU_BASE
#define CMU_SICLK		0x34 + CMU_BASE
#define CMU_SENSORCLK   0x20 + CMU_BASE
#define CMU_DISPLAYPLL  0x10 + CMU_BASE
#define CMU_BUSCLK1		0x38 + CMU_BASE

#define MFP_BASE 0xb01b0000
#define MFP_CTL3 0x4c + MFP_BASE
#define GPIO_COUTEN 0x18 + MFP_BASE
#define GPIO_CINEN 0x1c + MFP_BASE
#define GPIO_CDAT 0x20 + MFP_BASE

#define SYS_PG_CTL 0xb01b0100


void dump_regs(void){
	void __iomem *reg;
	int val;
	reg = ioremap(CMU_DEVCLKEN0 ,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "CMU_DEVCLKEN0", val);
	reg = ioremap(CMU_DEVCLKEN1 ,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "CMU_DEVCLKEN1", val);
	reg = ioremap(CMU_DEVRST0 ,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "CMU_DEVRST0", val);
	reg = ioremap(CMU_DEVRST1 ,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "CMU_DEVRST1", val);
	reg = ioremap(CMU_SICLK ,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "CMU_SICLK", val);
	reg = ioremap(CMU_SENSORCLK ,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "CMU_SENSORCLK", val);
	reg = ioremap(CMU_DISPLAYPLL ,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "CMU_DISPLAYPLL", val);
	reg = ioremap(CMU_BUSCLK1 ,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "CMU_BUSCLK1", val);

	reg = ioremap( MFP_CTL3,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "MFP_CTL3", val);
	reg = ioremap( GPIO_CINEN ,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "GPIO_CINEN", val);
	reg = ioremap( GPIO_COUTEN,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "GPIO_COUTEN", val);
	reg = ioremap( GPIO_CDAT,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "GPIO_CDAT", val);

	reg = ioremap(SYS_PG_CTL,4);
	val = readl(reg);
	pr_info("%s: = 0x%x", "SYS_PG_CTL", val);


	iounmap(reg);
}


static int caninos_si_probe(struct platform_device *pdev)
{
	int irq;
	struct caninos_si_dev *sidev;
	struct vb2_queue *q;
	struct resource *regs;
	//struct dma_chan *chan;
	//dma_cap_mask_t mask;
	int ret, i;

	pr_info("%s: init", __func__);
	sidev = devm_kzalloc(&pdev->dev, sizeof(struct caninos_si_dev), GFP_KERNEL);
	if (!sidev)
		return -ENOMEM;

    sidev->cam_param = devm_kzalloc(&pdev->dev, sizeof(struct caninos_camera_param), GFP_KERNEL);
	if (!sidev->cam_param)
		return -ENOMEM;

	sidev->hw_adapter = &atm7059_hw_adapter;

    ret = owl_camera_hw_call(sidev->hw_adapter, hw_adapter_init,pdev);

#ifdef MCU_K7
    sidev->sidev->rst = devm_reset_control_get(&pdev->dev, NULL);
	if (IS_ERR(sidev->sidev->rst))
	{
		dev_err(&pdev->dev, "could not get device reset control.\n");
		return -ENODEV;
	}
#endif

	ret = caninos_si_parse_dt(sidev, pdev);
	if(ret){
		pr_err("fail to parse SI parameters from dt!");
		return ret;
	}else
	{
		pr_info("SI parse dt ok");
	}

	ret = caninos_si_get_clock(&pdev->dev, sidev);
	if(ret){
		pr_err("fail to get SI clk!");
		return ret;
	}else
	{
		pr_info("SI get clks ok");
	}

    ret = caninos_si_init_device(sidev);
	if(ret){
		pr_err("fail to init SI dev!");
		return ret;
	}else
	{
		pr_info("SI dev init ok");
		dump_regs();
	}

	//dma_cap_zero(mask);
	//dma_cap_set(DMA_SLAVE, mask);
	
	//chan = dma_request_channel(mask, NULL, NULL);
	//if (!chan) {
	//	dev_info(&pdev->dev, "Unable to request DMA channel, defer probing\n");
	//	return -EPROBE_DEFER;
	//}

	sidev->dev = &pdev->dev;
	sidev->status = DEV_CLOSE;
	sidev->active = NULL;
	//sidev->cur_frm = NULL;
	//sidev->prev_frm = NULL;
	mutex_init(&sidev->mutex_lock);
	//mutex_init(&sidev->dma_lock);
	spin_lock_init(&sidev->lock);
	INIT_LIST_HEAD(&sidev->video_buffer_list);
    init_completion(&sidev->complete);

	//default values for camera parameters
	sidev->cam_param->isp_left=0;
	sidev->cam_param->isp_top=0;
	sidev->cam_param->width=640;
	sidev->cam_param->height=480;
	sidev->cam_param->pixel_code = MEDIA_BUS_FMT_UYVY8_2X8;
	sidev->cam_param->bus_type = V4L2_MBUS_PARALLEL;

	q = &sidev->queue;


	sidev->v4l2_dev.mdev = &sidev->mdev;

	/* Initialize media device */
	strscpy(sidev->mdev.model, "Caninos-SI", sizeof(sidev->mdev.model));
	snprintf(sidev->mdev.bus_info, sizeof(sidev->mdev.bus_info),
		 "platform:%s", "Caninos-SI");
	sidev->mdev.dev = &pdev->dev;
	media_device_init(&sidev->mdev);

	dev_set_drvdata(&pdev->dev, sidev);
	// Initialize the top-level structure
	ret = v4l2_device_register(&pdev->dev, &sidev->v4l2_dev);
	if(ret){
		pr_err("fail to register v4l2 dev!");
		return ret;
	}else
	{
		pr_info("register v4l2 dev ok");
	}

	sidev->vdev = video_device_alloc();
	if (!sidev->vdev) {
		ret = -ENOMEM;
		goto err_device_unregister;
	}else{
		pr_info("video alloc ok");
	}

	/* video node */
	sidev->vdev->fops = &caninos_si_fops;
	sidev->vdev->v4l2_dev = &sidev->v4l2_dev;
	sidev->vdev->queue = &sidev->queue;
	strscpy(sidev->vdev->name, KBUILD_MODNAME, sizeof(sidev->vdev->name));
	sidev->vdev->release = video_device_release;
	sidev->vdev->ioctl_ops = &caninos_si_ioctl_ops;
	sidev->vdev->lock = &sidev->mutex_lock;
	sidev->vdev->device_caps = V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_STREAMING; //| V4L2_CAP_READWRITE;
	video_set_drvdata(sidev->vdev, sidev);

	/* Media entity pads */
	sidev->vid_cap_pad.flags = MEDIA_PAD_FL_SINK;
	ret = media_entity_pads_init(&sidev->vdev->entity,
				     1, &sidev->vid_cap_pad);
	if (ret) {
		dev_err(sidev->dev, "Failed to init media entity pad\n");
		goto err_device_release;
	}
	sidev->vdev->entity.flags |= MEDIA_ENT_FL_DEFAULT;

	//register device at /dev/videoX, pick the first video device avaliable
	ret = video_register_device(sidev->vdev, VFL_TYPE_GRABBER, -1);
	if (ret) {
		dev_err(sidev->dev, "Failed to register video device\n");
		goto err_media_entity_cleanup;
	}

	dev_info(sidev->dev, "Device registered as %s\n",
		video_device_node_name(sidev->vdev));

	/* buffer queue */
	q->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    q->mem_ops = &vb2_dma_contig_memops;
	q->io_modes = VB2_MMAP | VB2_USERPTR;
    //q->io_modes = VB2_MMAP | VB2_USERPTR | VB2_READ | VB2_DMABUF;
	q->lock = &sidev->mutex_lock;//using queue lock this allow to use vb2_ops_wait_prepare
	q->drv_priv = sidev;
	q->buf_struct_size = sizeof(struct frame_buffer);
	q->ops = &caninos_videobuffer_ops;
	q->timestamp_flags = V4L2_BUF_FLAG_TIMESTAMP_MONOTONIC;
	q->min_buffers_needed = 2; //start_streaming won't be called until at lest 2 buffers on userspace  
	q->dev = &pdev->dev;

	ret = vb2_queue_init(q);
	if (ret < 0) {
		dev_err(&pdev->dev, "failed to initialize VB2 queue\n");
		goto err_media_entity_cleanup;
	}else{
		pr_info("vb2 queue init ok");
	}

	/*regs = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	sidev->regs = devm_ioremap_resource(&pdev->dev, regs);
	if (IS_ERR(sidev->regs)) {
		ret = PTR_ERR(sidev->regs);
		goto err_ioremap;
    }*/

	if (sidev->pdata.data_width_flags & SI_DATAWIDTH_8)
		sidev->width_flags = 1 << 7;
	if (sidev->pdata.data_width_flags & SI_DATAWIDTH_10)
		sidev->width_flags |= 1 << 9;

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		ret = irq;
		goto err_media_entity_cleanup;
	}

	ret = devm_request_irq(&pdev->dev, irq, caninos_si_isr, 0, "caninos-si", sidev);
	if (ret) {
		dev_err(&pdev->dev, "Unable to request irq %d\n", irq);
		goto err_media_entity_cleanup;
	}
	sidev->irq = irq;

	ret = caninos_si_graph_init(sidev);
	if (ret < 0){
		pr_info("si graph init fail");
		goto err_media_entity_cleanup;
	}else{
		pr_info("si graph init ok");
	}

	//pm_suspend_ignore_children(&pdev->dev, true);
	//pm_runtime_enable(&pdev->dev);
	platform_set_drvdata(pdev, sidev);
	return 0;


err_media_entity_cleanup:
	media_entity_cleanup(&sidev->vdev->entity);
err_device_release:
	video_device_release(sidev->vdev);
err_device_unregister:
	v4l2_device_unregister(&sidev->v4l2_dev);
err_media_device_cleanup:
	media_device_cleanup(&sidev->mdev);
	//dma_release_channel(sidev->dma_chan);

	return ret;
}

static int caninos_si_remove(struct platform_device *pdev)
{
	struct caninos_si_dev *sidev = platform_get_drvdata(pdev);

	//pm_runtime_disable(&pdev->dev);
	v4l2_async_notifier_unregister(&sidev->notifier);
	v4l2_async_notifier_cleanup(&sidev->notifier);
	v4l2_device_unregister(&sidev->v4l2_dev);
    owl_camera_hw_call(sidev->hw_adapter, hw_adapter_exit,pdev);
	return 0;
}

#ifdef CONFIG_PM
static int caninos_si_runtime_suspend(struct device *dev)
{
	struct caninos_si_dev *sidev = dev_get_drvdata(dev);

	clk_disable_unprepare(sidev->si_clk); //FIXME unprepare all clks
	//clk_disable_unprepare(sidev->pclk);
	//clk_disable_unprepare(sidev->pclk);
	return 0;
}
static int caninos_si_runtime_resume(struct device *dev)
{
	struct caninos_si_dev *sidev = dev_get_drvdata(dev);

	return clk_prepare_enable(sidev->si_clk);//FIXME prepare all clks
}
#endif // CONFIG_PM 

static const struct dev_pm_ops caninos_si_dev_pm_ops = {
	SET_RUNTIME_PM_OPS(caninos_si_runtime_suspend,
				caninos_si_runtime_resume, NULL)
};
static const struct of_device_id caninos_si_of_match[] = {
	{ .compatible = "caninos,k5-si" },
    { .compatible = "caninos,k7-si" },
	{ }
};
MODULE_DEVICE_TABLE(of, caninos_si_of_match);

static struct platform_driver caninos_si_driver = {
	.driver		= {
		.name = "caninos_si",
		.of_match_table = of_match_ptr(caninos_si_of_match),
		//.pm	= &caninos_si_dev_pm_ops,
	},
	.probe		= caninos_si_probe,
	.remove		= caninos_si_remove,
};


module_platform_driver(caninos_si_driver);

MODULE_DESCRIPTION("Caninos Camera Host driver");
MODULE_AUTHOR("Igor Ruschi Andrade E Lima <igor.lima@lsitec.org.br>");
MODULE_LICENSE("GPL");
MODULE_SUPPORTED_DEVICE("video");
